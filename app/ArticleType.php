<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleType extends Model
{
    protected $fillable = [
        'code', 'article_type', 'description', 'active_status'
    ];
}
