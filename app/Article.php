<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'title', 'content','short_description', 'thumbnail', 'articletype_id','date','time','postby','active_status'
    ];
}
