<?php

namespace App\Http\Controllers;
use File;
use App\User;
use App\Article;
use App\ArticleType;
use App\Helpers\LogActivity;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:article-list|articles-create|article-edit|article-delete', ['only' => ['index','show']]);
         $this->middleware('permission:article-create', ['only' => ['create','store']]);
         $this->middleware('permission:article-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:article-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $articles = Article::orderBy('id', 'desc')
               ->get();
        return view('backend.articles.index',compact('articles'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $articleTypes = ArticleType::where('active_status', '=',"1")->orderBy('id','DESC')->get();
        return view('backend.articles.create',compact('articleTypes'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required',
            'content' => 'required',
            'thumbnail'  =>  'required|file|image|mimes:jpeg,png,gif,jpg|max:2048'
        ]);    
        //$clientIP = \Request::getClientIp(true);        
        $title = $request->title;
        $detail= $request->content;
        
        $message=$request->content;
        $dom = new \DomDocument();
        $dom->loadHtml('<?xml encoding="UTF-8">'.$message);   
        $images = $dom->getElementsByTagName('img');

        foreach($images as $img){
            $src = $img->getAttribute('src');            
            // if the img source is 'data-url'
            if (preg_match("'/data:image/'", $src)) {
                preg_match("'/data:image\/(?<mime>.*?)\;/'", $src, $groups);
                $mimeType = $groups['mime'];
                $path = "'/images/'" . uniqid('', true) . '.' . $mimeType;
                Storage::disk('public')->put($path, file_get_contents($src));
                $image->removeAttribute('src');
                $image->setAttribute('src', Storage::disk('public')->url($path));
            }
        }

        // if ($request->file('thumbnail')->isValid()) {
        //     $file = $request->file('thumbnail');
        //     $fileName = 'thumbnail_'.uniqid().'.'.$file->getClientOriginalExtension();
        //     $pathImage = $file->storeAs('thumbnail', $fileName);
        // }
        $cover = $request->file('thumbnail');
        $covername = uniqid().'_thumbnail' .'.jpg';

        //if have an cover file
        if($cover){
            //storage the file on my public_covers (look my filesystems.php)
            Storage::disk('public')->put($covername, File::get($cover));
        }


        //
        $content = $dom->saveHTML();
        LogActivity::addLog('Create article: '.$title);

        Article::create([
            'title' => $title,
            'content' =>  $content,
            'short_description' => $request->short_description,
            'thumbnail' => $covername,
            'articletype_id' => $request->articletype,
            'date' => $request->date,
            'time' => $request->time,
            'postby' => "m",
            'active_status' => "1"
        ]);
        
        return redirect()->route('article.index')
                        ->with('success','Article has been created successfully.');
    
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $articles = Article::find($id);
        $articleTypes = ArticleType::whereIn('id', array($articles->articletype_id))->first();
        return view('backend.articles.show',compact('articles','articleTypes'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $articleTypes = ArticleType::where('active_status', '=',"1")->orderBy('id','DESC')->get();
        $articles = Article::find($id);
        return view('backend.articles.edit',compact('articleTypes','articles'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        request()->validate([
            'title' => 'required',
            'content' => 'required',
            'thumbnail'  =>  'required|file|image|mimes:jpeg,png,gif,jpg|max:2048'
        ]);      
        $title = $request->title;
        $detail= $request->content;
        
        $message=$request->content;
        $dom = new \DomDocument();
        $dom->loadHtml('<?xml encoding="UTF-8">'.$message);   
        $images = $dom->getElementsByTagName('img');

        foreach($images as $img){
            $src = $img->getAttribute('src');        
            if (preg_match("'/data:image/'", $src)) {
                preg_match("'/data:image\/(?<mime>.*?)\;/'", $src, $groups);
                $mimeType = $groups['mime'];
                $path = "'/images/'" . uniqid('', true) . '.' . $mimeType;
                Storage::disk('public')->put($path, file_get_contents($src));
                $image->removeAttribute('src');
                $image->setAttribute('src', Storage::disk('public')->url($path));
            }
        }

        $cover = $request->file('thumbnail');
        $covername = uniqid().'_thumbnail' .'.jpg';

        if($cover){
            Storage::disk('public')->put($covername, File::get($cover));
        }

        $content = $dom->saveHTML();
        LogActivity::addLog('Update article: '.$title);

        $input = $request->all();

        $user = Article::find($id);
        $user->update($input); 

        // Article::create([
        //     'title' => $title,
        //     'content' =>  $content,
        //     'short_description' => $request->short_description,
        //     'thumbnail' => $covername,
        //     'articletype_id' => $request->articletype,
        //     'date' => $request->date,
        //     'time' => $request->time,
        //     'postby' => "m",
        //     'active_status' => "1"
        // ]);
        
        return redirect()->route('article.index')
                        ->with('success','Article has been updated successfully.');
    
    }


    public function status(Request $request,$id){
        Article::where('id', $id)->update(array(
            'active_status' 	  =>  $request->status
        ));
        return redirect()->route('article.index')
        ->with('success','Article has been updated (status) successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Article::find($id)->delete();
        return redirect()->route('article.index')
                        ->with('success','Article has been deleted successfully');
    }

    /**
     * Create a thumbnail of specified size
     *
     * @param string $path path of thumbnail
     * @param int $width
     * @param int $height
     */
   
  
}