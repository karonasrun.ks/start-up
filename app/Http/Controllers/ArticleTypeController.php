<?php

namespace App\Http\Controllers;

use App\ArticleType;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;

class ArticleTypeController extends Controller
{

    function __construct()
    {
         $this->middleware('permission:articletype-list|articletype-create|articletype-edit|articletype-delete', ['only' => ['index','store']]);
         $this->middleware('permission:articletype-create', ['only' => ['create','store']]);
         $this->middleware('permission:articletype-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:articletype-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articleTypes = ArticleType::orderBy('id','DESC')->get();
        return view('backend.articles_type.index',compact('articleTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.articles_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'code' => 'required',
            'article_type' => 'required',
            'description' => 'required'
        ]);
        $user = ArticleType::where('article_type', '=', $request->article_type)->first();
        if ($user === null) {   
            ArticleType::create($request->all());
            return redirect()->route('articletype.index')
                        ->with('success','Article type has been created successfully.');
        }else{
                return Redirect::back() ->with('error','Article type is has already exist!');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ArticleType  $articleType
     * @return \Illuminate\Http\Response
     */
    public function show(ArticleType $articleType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ArticleType  $articleType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $articleTypes = ArticleType::find($id);
        return view('backend.articles_type.edit',compact('articleTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ArticleType  $articleType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $user = ArticleType::find($id);
        $user->update($input);
        return redirect()->route('articletype.index')
        ->with('success','Article type has been updated successfully.');
    }

    public function status(Request $request,$id){
            ArticleType::where('id', $id)->update(array(
                'active_status' 	  =>  $request->status
            ));
            return redirect()->route('articletype.index')
            ->with('success','Article type has been updated (status) successfully.');
    
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ArticleType  $articleType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArticleType $articleType)
    {
        //
    }
}
