<?php

namespace App\Http\Controllers;

use App\User;
use App\Article;
use App\ArticleType;
use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    public function home(){
        $articleTypes = ArticleType::orderBy('id','DESC')->get();
        $articles = Article::orderBy('id','DESC')->get();
        return view('frontend.home',compact('articleTypes','articles'));
    }

    public function sport(){
        $header = "ព័ត៌មានកីឡា";
        $style = "page-header-sport";
        $articleTypes = ArticleType::orderBy('id','DESC')->get();
        return view('frontend.list',compact('articleTypes','header','style'));
    }

    public function sportDetail(){
        return view('frontend.show');
    }

    public function technology(){
        $header = "ព័ត៌មានបច្ចេកវិទ្យា";
        $style = "page-header-technology";
        $articleTypes = ArticleType::orderBy('id','DESC')->get();
        return view('frontend.list',compact('articleTypes','header','style'));
    }

    public function technologyDetail(){
        return view('frontend.show');
    }

    public function life(){
        $header = "ព័ត៌មានជីវិតនិងសង្គម";
        $style = "page-header-life";
        $articleTypes = ArticleType::orderBy('id','DESC')->get();
        return view('frontend.list',compact('articleTypes','header','style'));
    }

    public function lifeDetail(){
        return view('frontend.show');
    }

    public function entertainment(){
        $header = "ព័ត៌មានកម្សាន្ត";
        $style = "page-header-entertainment";
        $articleTypes = ArticleType::orderBy('id','DESC')->get();
        return view('frontend.list',compact('articleTypes','header','style'));
    }

    public function entertainmentDetail(){
        return view('frontend.show');
    }

    public function healthyLife(){
        $header = "ព័ត៌មានសុខភាពនិងជីវិត";
        $style = "page-header-hlife";
        $articleTypes = ArticleType::orderBy('id','DESC')->get();
        return view('frontend.list',compact('articleTypes','header','style'));
    }

    public function healthyLifeDetail(){
        return view('frontend.show');
    }

    public function hangOut(){
        $header = "ព័ត៌មានទេសចរណ៍";
        $style = "page-header-hangout";
        $articleTypes = ArticleType::orderBy('id','DESC')->get();
        return view('frontend.list',compact('articleTypes','header','style'));
    }

    public function hangOutDetail(){
        return view('frontend.show');
    }
    

    public function about()
    {
        return view('frontend.aboutus');
    }

    public function contact()
    {
        return view('frontend.contactus');
    }
}
