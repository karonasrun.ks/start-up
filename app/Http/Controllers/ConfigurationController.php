<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class ConfigurationController extends Controller
{

    public function index()
    {
        return view('backend.configs.index');
    }
  
    public function view()
    {
        Artisan::call('view:clear');
        return view('backend.configs.index')->with('success','Role created successfully');
    }

    public function config()
    {
        Artisan::call('view:config');
        return view('backend.configs.index')->with('success','Role created successfully');
    }

    public function route()
    {
        Artisan::call('view:route');
        return view('backend.configs.index')->with('success','Role created successfully');
    }

    public function cache()
    {
        Artisan::call('view:cache');
        return view('backend.configs.index')->with('success','Role created successfully');
    }

}
