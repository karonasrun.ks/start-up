<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogActivity extends Model
{
    protected $fillable = [
        'description', 'url', 'method', 'ip', 'agent', 'user_id'
    ];
}
