<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
|run cmd
|php artisan make:auth
|php artisan migrate
|php artisan db:seed --class=PermissionTableSeeder
|php artisan db:seed --class=CreateAdminUserSeeder
|
|
*/
// Route test
// Route::get('/', function () {
//     return view('layouts.frontend');
// });

Route::get('/', 'FrontEndController@home');
Route::get('/sport','FrontEndController@sport');
Route::get('/sport-detail','FrontEndController@sportDetail');
Route::get('/technology','FrontEndController@technology');
Route::get('/technology-detail','FrontEndController@technologyDetail');
Route::get('/life','FrontEndController@life');
Route::get('/life-detail','FrontEndController@lifeDetail');
Route::get('/healthy-life','FrontEndController@healthyLife');
Route::get('/healthy-life-detail','FrontEndController@healthyLifeDetail');
Route::get('/entertainment','FrontEndController@entertainment');
Route::get('/entertainment-detail','FrontEndController@entertainmentDetail');
Route::get('/hang-out','FrontEndController@hangOut');
Route::get('/hang-out-detail','FrontEndController@hangOutDetail');

Route::get('/about', 'FrontEndController@about');
Route::get('/contact', 'FrontEndController@contact');

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false,
  ]);

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function() {    

    Route::resource('article', 'ArticleController');
    Route::post('article/status/{id}', 'ArticleController@status');
    Route::resource('articletype', 'ArticleTypeController');
    Route::post('articletype/status/{id}', 'ArticleTypeController@status');
    Route::resource('user','UserController');
    Route::resource('role' , 'RoleController');

    // Configuration
    Route::get('configuration', 'ConfigurationController@index');

     //Clear route cache:
    Route::get('/clear-route-cache', function() {
        $exitCode = Artisan::call('route:cache');
        return back()->with('success','You has been clearred route successfully');
    });

    //Clear config cache:
    Route::get('/clear-cache-config', function() {
        $exitCode = Artisan::call('config:cache');
        return back()->with('success','You has been clearred config successfully');
    }); 

    // Clear application cache:
    Route::get('/clear-cache', function() {
        $exitCode = Artisan::call('cache:clear');
        return back()->with('success','You has been clearred cache successfully');
    });

    // Clear view cache:
    Route::get('/clear-view', function() {
        $exitCode = Artisan::call('view:clear');
        return back()->with('success','You has been clearred view successfully');
    });
});

Route::get('addlog', 'HomeController@addLog');
Route::get('logactivity', 'HomeController@logActivity');

//Multiple role & permission
//https://www.itsolutionstuff.com/post/laravel-58-user-roles-and-permissions-tutorialexample.html
