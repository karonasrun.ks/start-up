<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>818info</title>

<meta name="keywords" content="HTML5 Template">
<meta name="description" content="818info">
<meta name="author" content="Karona">

<!-- Favicon -->
<link rel="shortcut icon" href="" type="image/x-icon">
<link rel="apple-touch-icon" href="">

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css') }}">

<link rel="stylesheet" href="{{ asset('css/font.css') }}">
<link rel="stylesheet" href="{{ asset('css/f/fowl.carousel.min.css') }}">
<!-- Vendor CSS -->
<link rel="stylesheet" href="{{ asset('css/f/fbootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/f/fanimate.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/f/fsimple-line-icons.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/f/fowl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/f/fowl.theme.default.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/f/fmagnific-popup.min.css') }}">
<!-- Theme CSS -->
<link rel="stylesheet" href="{{ asset('css/f/ftheme.css') }}">
<link rel="stylesheet" href="{{ asset('css/f/ftheme-elements.css') }}">
<!-- Current Page CSS -->
<link rel="stylesheet" href="{{ asset('css/f/component.css') }}">
<link rel="stylesheet" href="{{ asset('css/f/nivo-slider.css') }}">
<!-- Skin CSS -->
<link rel="stylesheet" href="{{ asset('css/f/default.css') }}">
</head>

<body data-gr-c-s-loaded="true">
    <div class="body">
        <header id="header" data-plugin-options="{&#39;stickyEnabled&#39;: true, &#39;stickyEnableOnBoxed&#39;: true, &#39;stickyEnableOnMobile&#39;: true, &#39;stickyStartAt&#39;: 57, &#39;stickySetTop&#39;: &#39;-57px&#39;, &#39;stickyChangeLogo&#39;: true}" style="min-height: 125px;">
            <div class="header-body" style="top: 0px;">
                <div class="header-container container">
                    <div class="header-row">
                        <div class="header-column">
                            <div class="header-logo" style="width: 126px; height: 84px;">
                                <a href="http://preview.oklerthemes.com/porto/5.7.2/index.html">
                                    <img alt="Porto" width="111" height="54" data-sticky-width="82" data-sticky-height="40" data-sticky-top="33" src="{{ asset('img/logo-default.png') }}" style="top: 0px; width: 111px; height: 54px;">
                                </a>
                            </div>
                        </div>
                        <div class="header-column">
                            <div class="header-row">
                                
                                <nav class="header-nav-top">
                                    <ul class="nav nav-pills">
                                        <li class="hidden-xs">
                                            <a href="{{ url('about') }}"><i class="fa fa-angle-right"></i> About Us</a>
                                        </li>
                                        <li class="hidden-xs">
                                            <a href="{{ url('contact') }}"><i class="fa fa-angle-right"></i> Contact Us</a>
                                        </li>
                                        <li>
                                            <span class="ws-nowrap"><i class="fa fa-phone"></i> (855) 12 387 438</span>
                                        </li>
                                        <li class="social-icons-facebook">
                                           <a href="{{ url('/login') }}" class="mb-xs mt-xs mr-xs btn btn-default"><i class="fas fa-sign-in-alt"></i> ចូលគណនី</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="header-row">
                                <div class="header-nav">
                                    <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                                        <i class="fa fa-bars"></i>
                                    </button>
                                    <ul class="header-social-icons social-icons hidden-xs">
                                        <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                                        <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                                        <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                                        <li class="social-icons-youtube"><a href="http://www.youtube.com/" target="_blank" title="Youtube"><i class="fab fa-youtube"></i></a></li>
                                    </ul>
                                    <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
                                        <nav>
                                            <ul class="nav nav-pills" id="mainNav">
                                                <li class="active">
                                                <a href="{{ url('/') }}">
                                                                ទំព័រដើម</a>
                                                </li>
                                                <li class="dropdown">
                                                        <a class="dropdown-toggle" href="#information">
                                                            ព័ត៌មាន
                                                                <i class="fa fa-caret-down"></i></a>
                                                        <ul class="dropdown-menu">
                                                            @foreach ($articleTypes as $articleType)
                                                                <li><a href="{{ $articleType->code}}">{{ $articleType->article_type}}</a></li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                <li class="dropdown dropdown-mega">
                                                    <a class="dropdown-toggle" href="http://preview.oklerthemes.com/porto/5.7.2/index.html#">
                                                                ចំណេះដឹង
                                                            <i class="fa fa-caret-down"></i></a>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <div class="dropdown-mega-content">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <span class="dropdown-mega-sub-title">Shortcodes 1</span>
                                                                        <ul class="dropdown-mega-sub-nav">
                                                                            <li><a href="http://preview.oklerthemes.com/porto/5.7.2/shortcodes-accordions.html">Accordions</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <span class="dropdown-mega-sub-title">Shortcodes 2</span>
                                                                        <ul class="dropdown-mega-sub-nav">
                                                                            <li><a href="http://preview.oklerthemes.com/porto/5.7.2/shortcodes-buttons.html">Buttons</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <span class="dropdown-mega-sub-title">Shortcodes 3</span>
                                                                        <ul class="dropdown-mega-sub-nav">
                                                                            <li><a href="http://preview.oklerthemes.com/porto/5.7.2/shortcodes-call-to-action.html">Call to Action</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <span class="dropdown-mega-sub-title">Shortcodes 4</span>
                                                                        <ul class="dropdown-mega-sub-nav">
                                                                            <li><a href="http://preview.oklerthemes.com/porto/5.7.2/shortcodes-headings.html">Headings</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </li> 
                                               <li>
                                                    <form action="{{ url('/') }}" method="post">
                                                        <div class="searchbar">
                                                                <input class="search_input" type="text" name="" placeholder="Search...">
                                                                <a href="#" class="search_icon"><i class="fas fa-search"></i></a>
                                                              </div>                                   
                                                            </form>         
                                                    </div> 
                                                </li>                                               
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        {{-- MAIN CONTENT --}}
        @yield('content')
        {{-- END MAIN CONTENT --}}
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="footer-ribbon">
                        <span>Get in Touch</span>
                    </div>
                    <div class="col-md-3">
                        <div class="newsletter">
                            <h4>Newsletter</h4>
                            <p>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.</p>

                            <div class="alert alert-success hidden" id="newsletterSuccess">
                                <strong>Success!</strong> You've been added to our email list.
                            </div>

                            <div class="alert alert-danger hidden" id="newsletterError"></div>

                            <form id="newsletterForm" action="#submit" method="POST" novalidate="novalidate">
                                <div class="input-group">
                                    <input class="form-control" placeholder="Email Address" name="newsletterEmail" id="newsletterEmail" type="text">
                                    <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit">Go!</button>
                                            </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h4>Latest Tweets</h4>
                        <div id="tweet" class="twitter" data-plugin-tweets="" data-plugin-options="{&#39;username&#39;: &#39;oklerthemes&#39;, &#39;count&#39;: 2}">
                            <ul>
                                <li><span class="status"><i class="fa fa-twitter"></i> If you have any suggestions for the next updates, let us know.</span><span class="meta"> <a href="#" target="_blank">10:05 AM Sep 18th</a></span></li>
                                <li><span class="status"><i class="fa fa-twitter"></i> We have just updated Porto Admin. Check the changelog for more information.</span><span class="meta"> <a href="#" target="_blank">10:04 AM Sep 18th</a></span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="contact-details">
                            <h4>Contact Us</h4>
                            <ul class="contact">
                                <li>
                                    <p><i class="fa fa-map-marker"></i> <strong>Address:</strong> Phnom Penh, Cambodia</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-phone"></i> <strong>Phone:</strong> (855) 12 387 438</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:karonasrun.ks@gmail.com">karonasrun.ks@gmail.com</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <h4>Follow Us</h4>
                        <ul class="social-icons">
                            <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                            <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                            <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                            <li class="social-icons-youtube"><a href="http://www.youtube.com/" target="_blank" title="Youtube"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-md-1">
                        <a href="{{ url('/') }}" class="logo">
                                <img alt="Porto Website Template" class="img-responsive" src="{{ asset('img/logo-footer.png') }}">
                            </a>
                        </div>
                        <div class="col-md-7">
                            <p>© Copyright 2017. All Rights Reserved.</p>
                        </div>
                        <div class="col-md-4">
                            <nav id="sub-menu">
                                <ul>
                                    <li><a href="#faq">FAQ's</a></li>
                                    <li><a href="#map">Sitemap</a></li>
                                    <li><a href="{{ url('contact') }}">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- Vendor -->
    <script src="{{ asset('js/f/jquery.min.js') }}"></script>
    <script src="{{ asset('js/f/jquery.appear.min.js') }}"></script>
    <script src="{{ asset('js/f/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/f/jquery-cookie.min.js') }}"></script>
    <script src="{{ asset('js/f/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/f/common.min.js') }}"></script>
    <script src="{{ asset('js/f/jquery.validation.min.js') }}"></script>
    <script src="{{ asset('js/f/jquery.easy-pie-chart.min.js') }}"></script>
    <script src="{{ asset('js/f/jquery.gmap.min.js') }}"></script>
    <script src="{{ asset('js/f/jquery.lazyload.min.js') }}"></script>
    <script src="{{ asset('js/f/jquery.isotope.min.js') }}"></script>
    <script src="{{ asset('js/f/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/f/jquery.magnific-popup.min.js') }}"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="{{ asset('js/f/theme.js') }}"></script>

    <!-- Current Page Vendor and Views -->
    <script src="{{ asset('js/f/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('js/f/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('js/f/jquery.flipshow.min.js') }}"></script>
    <script src="{{ asset('js/f/jquery.nivo.slider.min.js') }}"></script>
	<script src="{{ asset('js/f/view.home.js') }}"></script>

    <!-- Theme Custom -->
    <script src="{{ asset('js/f/custom.js') }}"></script>

    <!-- Theme Initialization Files -->
    <script src="{{ asset('js/f/theme.init.js') }}"></script>
    <a class="scroll-to-top hidden-mobile" href="{{ url('/') }}"><i class="fas fa-chevron-up"></i></a>
<script>
$(document).ready(function() {
    $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
});
</script>
</body>

</html>