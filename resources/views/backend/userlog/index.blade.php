@extends('layouts.backend') @section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Dashboard</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Log Activity</a>
                        </li>
                        <li class="breadcrumb-item active">Log Activity Lists</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
        {{-- --}}
        <div class="row">
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
                @endif
                <div class="card card-outline card-info">
                    <div class="card-header">
                        <h3 class="card-title">
                            Log Activity
                            <small>List</small>
                        </h3>
                        <!-- tools box -->
                        <div class="card-tools">
                            {{-- <a href="{{ route('article.create') }}" class="btn btn-sm btn-primary">
                  <i class="fas fa-plus-circle"></i> Create New</a> --}}
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body pad">
                        <table id="example1" class="table table-bordered table-striped dataTable">
                            <tr>
                                <th>No</th>
                                <th>Description</th>
                                <th>URL</th>
                                <th>Method</th>
                                <th>Ip</th>
                                <th width="300px">User Agent</th>
                                <th>User Id</th>
                                <th style="width: 9%;">Action</th>
                            </tr>
                            @if($logs->count())
                                @foreach($logs as $key => $log)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $log->description }}</td>
                                    <td class="text-success">{{ $log->url }}</td>
                                    <td><label class="label label-info">{{ $log->method }}</label></td>
                                    <td class="text-warning">{{ $log->ip }}</td>
                                    <td class="text-danger">{{ $log->agent }}</td>
                                    <td>{{ $log->user_id }}</td>
                                    <td><button class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</button></td>
                                </tr>
                                @endforeach
                            @endif    
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection @section('js')
<script type="text/javascript">
    $(function () {
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "fixedHeader": {
                "header": true,
                "headerOffset": $('#header').height()
            },
            "oLanguage": {
                "sSearch": "Filter results:"
            },
            "responsive": false,
            "select": true,
            "destroy": true,
        });
      });
</script>
@endsection