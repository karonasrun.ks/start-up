@extends('layouts.backend')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Dashboard</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item">
							<a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="{{ url('configuration') }}">Configuration</a>
                        </li>
						<li class="breadcrumb-item active">Configuration List</li>
					</ol>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
    </section>
   
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-12">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                    <p>{{ $message }}</p>
                    </div>
                @endif				
				<div class="card">
					<div class="card-header">
                        <h3 class="card-title">Configuration List
                    </h3>
                    </div>
					<!-- /.card-header -->
					<div class="card-body">
                            <div class="card-body">
                                    <p>List of the cache clear commands.</p>
                            <a href="{{ url('/clear-view') }}" class="btn btn-sm btn-default">
                                        <i class="fas fa-cog"></i> Clear View
                                    </a>
                                    <a href="{{ url('/clear-cache') }}" class="btn btn-sm btn-default">
                                        <i class="fas fa-cog"></i> Clear Cache
                                    </a>
                                    <a href="{{ url('/clear-route-cache') }}" class="btn btn-sm btn-default">
                                        <i class="fas fa-cog"></i> Clear Route
                                    </a>
                                    <a href="{{ url('/clear-cache-config') }}" class="btn btn-sm btn-default">
                                        <i class="fas fa-cogs"></i> Clear Configuration
                                    </a>                                    
                                  </div>                       
					</div>
					<!-- /.card-body -->
				</div>
                <!-- /.card -->                
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
      
@endsection
@section('js')			
<script type="text/javascript">
$(function () {
    $('#example1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "fixedHeader": {
            "header": true,
            "headerOffset": $('#header').height()
        },
        "oLanguage": {
            "sSearch": "Filter results:"
        },
        "responsive": false,
        "select": true,
        "destroy": true,
    });
  });
</script>
@endsection