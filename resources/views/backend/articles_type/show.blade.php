@extends('layouts.backend')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Dashboard</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item">
							<a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="{{ url('users') }}">User</a>
                        </li>
						<li class="breadcrumb-item active">Users List</li>
					</ol>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
    </section>
    
	<!-- Main content -->

	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">				
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Users List
							<a href="{{ route('users.create') }}" class="btn btn-success btn-sm pull-left"><i class="fas fa-plus"></i> Create New user</a>
							</h3>
						</div>
						<!-- /.card-header -->
						<div class="card-body">
								@if (count($errors) > 0)
								<div class="alert alert-danger">
									<strong>Whoops!</strong> There were some problems with your input.<br><br>
									<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
									</ul>
								</div>
								@endif
                                <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <strong>Name:</strong>
                                                {{ $user->username }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <strong>Email:</strong>
                                                {{ $user->email }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <strong>Roles:</strong>
                                                @if(!empty($user->getRoleNames()))
                                                    @foreach($user->getRoleNames() as $v)
                                                        {{ $v }}
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>


		
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->                
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
      
@endsection
@section('js')		
@endsection