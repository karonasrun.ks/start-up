@extends('layouts.backend')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Dashboard</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item">
							<a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="#">Articles type</a>
                        </li>
						<li class="breadcrumb-item active">Articles Type List</li>
					</ol>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</section>
	
    
	<!-- Main content -->
	<section class="content">
		{{--  --}}
<div class="row">
        <div class="col-md-12">
				@if ($message = Session::get('success'))
				<div class="alert alert-success">
				<p>{{ $message }}</p>
				</div>
			@endif
          <div class="card card-outline card-dark">
            <div class="card-header">
              <h3 class="card-title">
                Article Type
                <small>List</small>
              </h3>
              <!-- tools box -->
              <div class="card-tools">
                <a href="{{ route('articletype.create') }}" class="btn btn-sm btn-primary" >
                  <i class="fas fa-plus-circle"></i> Create New</a>                
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body pad">
					<table id="example1" class="table table-bordered table-striped dataTable">
							<thead>
								<tr>
									<th>No</th>
									<th>Article Type</th>
									<th>Description</th>
                                    <th style="width: 27%;"></th>
								</tr>
							</thead>
							<tbody>	
                            @foreach ($articleTypes as $key => $articleType)
							<tr>
									<td>{{ ++$key }}</td>
									<td>{{ $articleType->article_type }}</td>
									<td>{{ $articleType->description }}</td>
                                    <td>
											<form action="{{ url('articletype/status',$articleType->id) }}" method="post" style="display:inline">
													@csrf
													@if($articleType->active_status == 1)
														<input type="hidden" name="status" value="0">
													<button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-toggle-off"></i> Deactivated</button>
													@else
														<input type="hidden" name="status" value="1">
													<button type="submit" class="btn btn-success btn-sm"><i class="fas fa-toggle-on"></i> Activated</button>
													@endif
													
												</form>
										<a href="{{ route('articletype.show',$articleType->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-folder"></i> Show</a>
										<a href="{{ route('articletype.edit',$articleType->id) }}" class="btn btn-warning btn-sm"><i class="fas fa-pencil-alt"></i> Edit</a>										
									</td>
								</tr>
							@endforeach
                            </tbody>
                        </table>    
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
      
@endsection
@section('js')			
<script type="text/javascript">
$(function () {
    $('#example1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "fixedHeader": {
            "header": true,
            "headerOffset": $('#header').height()
        },
        "oLanguage": {
            "sSearch": "Filter results:"
        },
        "responsive": false,
        "select": true,
        "destroy": true,
    });
  });
</script>
@endsection