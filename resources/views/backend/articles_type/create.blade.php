@extends('layouts.backend')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Dashboard</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item">
							<a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="{{ url('articletype') }}">User</a>
                        </li>
						<li class="breadcrumb-item active">Users List</li>
					</ol>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
    </section>
    
	<!-- Main content -->

	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">				
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Users List
							<a href="{{ route('articletype.index') }}" class="btn btn-success btn-sm pull-left"><i class="fas fa-list-alt"></i> List</a>
							</h3>
						</div>
						<!-- /.card-header -->
						@if ($message = Session::get('error'))
							<div class="alert alert-danger">
							<p>{{ $message }}</p>
							</div>
						@endif
					<form class="form-horizontal"​​​ action="{{ route('articletype.store') }}" method="POST">
						@csrf
							<div class="card-body">
							  <div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 col-form-label"><i class="text-red">*</i> Code:</label>
								<div class="col-sm-5">
								  <input type="text" class="form-control" name="code" id="inputEmail3" placeholder="AT-01">
								</div>
							  </div>
							  <div class="form-group row">
								<label for="inputPassword3" class="col-sm-2 col-form-label"><i class="text-red">*</i> Article Type:</label>
								<div class="col-sm-5">
								  <input type="text" class="form-control" name="article_type" id="inputPassword3" placeholder="Sport">
								</div>
							  </div>
							  <div class="form-group row">
								<label for="inputPassword3" class="col-sm-2 col-form-label"><i class="text-red">*</i> Description:</label>
								<div class="col-sm-5">
									<textarea class="form-control" rows="3" cols="4" name="description" placeholder="Enter ..."></textarea>
								</div>
							  </div>
							  <div class="form-group row">
								<label for="inputPassword3" class="col-sm-2 col-form-label">Status:</label>
								<div class="col-sm-5">
									<select name="active_status" class="form-control">
										<option value="1">Activated</option>
										<option value="0">Deactivated</option>
									</select>
								</div>
							  </div>
							  
							</div>
							<!-- /.card-body -->
							<div class="card-footer">
									<button type="submit" class="btn btn-info btn-sm col-sm-1"><i class="fas fa-save"></i>  Save</button>
									<button type="reset" class="btn btn-danger btn-sm col-sm-1"><i class="fas fa-trash-restore"></i>  Cancel</button>
								  </div>
							<!-- /.card-footer -->
						  </form>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->                
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
      
@endsection
@section('js')		
@endsection