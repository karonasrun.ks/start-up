@extends('layouts.backend')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Dashboard</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item">
							<a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="{{ url('articletype') }}">Article Type</a>
                        </li>
						<li class="breadcrumb-item active">Article Type List</li>
					</ol>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
    </section>
    
	<!-- Main content -->

	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">				
					<div class="card card-outline card-dark">
							<div class="card-header">
							  <h3 class="card-title">
								Article Type
								<small>Edit</small>
							  </h3>
							  <!-- tools box -->
							  <div class="card-tools">
								<a href="{{ route('articletype.index') }}" class="btn btn-sm btn-primary" >
								  <i class="fas fa-list"></i> List</a>                
							  </div>
							  <!-- /. tools -->
							</div>
							<!-- /.card-header -->
							<div class="card-body pad">
					<form class="form-horizontal"​​​ action="{{ route('articletype.update', $articleTypes->id) }}" method="POST" enctype="multipart/form-data">
							@csrf
							@method('PUT')
							<div class="card-body">
							  <div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 col-form-label">Code:</label>
								<div class="col-sm-5">
								<input type="text" class="form-control" name="code" id="inputEmail3" value="{{ $articleTypes->code}}">
								</div>
							  </div>
							  <div class="form-group row">
								<label for="inputPassword3" class="col-sm-2 col-form-label">Article Type:</label>
								<div class="col-sm-5">
								  <input type="text" class="form-control" name="article_type" id="inputPassword3" value="{{ $articleTypes->article_type}}">
								</div>
							  </div>
							  <div class="form-group row">
								<label for="inputPassword3" class="col-sm-2 col-form-label">Description:</label>
								<div class="col-sm-5">
									<textarea class="form-control" rows="3" cols="4" name="description">{{ $articleTypes->description}}</textarea>
								</div>
							  </div>
							  <div class="form-group row">
								<label for="inputPassword3" class="col-sm-2 col-form-label">Status:</label>
								<div class="col-sm-5">
									<select name="active_status" class="form-control">
										<option value="1">Activated</option>
										<option value="0">Deactivated</option>
									</select>
								</div>
							  </div>
							  
							</div>
							<!-- /.card-body -->
							<div class="card-footer">
							  <button type="submit" class="btn btn-info btn-sm col-sm-1"><i class="fas fa-save"></i>  Save</button>
							  <button type="reset" class="btn btn-danger btn-sm col-sm-1"><i class="fas fa-trash-restore"></i>  Cancel</button>
							</div>
							<!-- /.card-footer -->
						  </form>
						</div>
					</div>
				  </div>
				  <!-- /.col-->
				</div>
		  
			  </section>
			  <!-- /.content -->
		  </div>
      
@endsection
@section('js')		
@endsection