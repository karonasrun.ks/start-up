@extends('layouts.backend')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Dashboard</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item">
							<a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="{{ url('roles') }}">Role</a>
                        </li>
						<li class="breadcrumb-item active">Roles List</li>
					</ol>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
    </section>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
        <p>{{ $message }}</p>
        </div>
    @endif
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-12">				
				<div class="card">
					<div class="card-header">
                        <h3 class="card-title">Create New Role
                        {{-- @can('role-create') --}}
                        <a href="{{ route('roles.index') }}" class="btn btn-success btn-sm pull-left"> Roles List</a>
                        {{-- @endcan     --}}
                    </h3>
                    </div>
					<!-- /.card-header -->
					<div class="card-body">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Name:</strong>
                                        {{ $role->name }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Permissions:</strong>
                                        @if(!empty($rolePermissions))
                                            @foreach($rolePermissions as $v)
                                                <span class="badge bg-success">{{ $v->name }}</span>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>     
					</div>
					<!-- /.card-body -->
				</div>
                <!-- /.card -->                
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
      
@endsection
@section('js')		
@endsection