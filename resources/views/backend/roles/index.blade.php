@extends('layouts.backend')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Dashboard</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item">
							<a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="{{ url('role') }}">Role</a>
                        </li>
						<li class="breadcrumb-item active">Roles List</li>
					</ol>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
    </section>
  
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-12">	
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                    <p>{{ $message }}</p>
                    </div>
                @endif			
				<div class="card">
					<div class="card-header">
                        <h3 class="card-title">Roles List
                        {{-- @can('role-create') --}}
                        <a href="{{ route('role.create') }}" class="btn btn-success btn-sm pull-left"> Create New Role</a>
                        {{-- @endcan     --}}
                    </h3>
                    </div>
					<!-- /.card-header -->
					<div class="card-body">
						<table id="example1" class="table table-bordered table-striped dataTable">
							<thead>
								<tr>
									<tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th width="280px">Action</th>
                                    </tr>
								</tr>
							</thead>
							<tbody>	
                                    @foreach ($roles as $key => $role)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $role->name }}</td>
                                        <td>
                                            <a class="btn btn-info btn-sm" href="{{ route('role.show',$role->id) }}"><i class="fas fa-eye"></i> Show</a>
                                            @can('role-edit')
                                                <a class="btn btn-primary btn-sm" href="{{ route('role.edit',$role->id) }}"><i class="fas fa-pencil-alt"></i> Edit</a>
                                            @endcan
                                            @can('role-delete')
                                                {!! Form::open(['method' => 'DELETE','route' => ['role.destroy', $role->id],'style'=>'display:inline']) !!}
                                                @csrf
                                                    {{ Form::button('<i class="fa fa-trash"></i> submit', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm'] )  }}
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>                        
					</div>
					<!-- /.card-body -->
				</div>
                <!-- /.card -->                
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
      
@endsection
@section('js')			
<script type="text/javascript">
$(function () {
    
    $('#example1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "fixedHeader": {
            "header": true,
            "headerOffset": $('#header').height()
        },
        "oLanguage": {
            "sSearch": "Filter results:"
        },
        "responsive": false,
        "select": true,
        "destroy": true,
    });

    $('.toastrDefaultSuccess').click(function() {
      toastr.success('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
    });
    $('.toastrDefaultInfo').click(function() {
      toastr.info('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
    });
    $('.toastrDefaultError').click(function() {
      toastr.error('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
    });
    $('.toastrDefaultWarning').click(function() {
      toastr.warning('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
    });

  });
</script>
@endsection