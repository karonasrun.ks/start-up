@extends('layouts.backend') @section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Dashboard</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ url('article') }}">Article</a>
                        </li>
                        <li class="breadcrumb-item active">Article Show</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    <!-- Main content -->

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">
                                Article
                                <small></small>
                            </h3>
                            <!-- tools box -->
                            <div class="card-tools">
                                <a href="{{ route('article.index') }}" class="btn btn-primary btn-sm">
							  <i class="fas fa-list"></i> Article List</a>
                            </div>
                            <!-- /. tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body pad">
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        	<div class="form-group">
                                <label>Title</label>
                                <p>{{ $articles->title }}</p>
                            </div>
                            <div class="form-group">
                                <label>Type</label>
                                <p>{{ $articleTypes->article_type }}</p>
                            </div>
                            <div class="form-group">
                                <label>Thumbnail</label><br>
                                <img src="{{ asset('storage/'.$articles->thumbnail) }}" width="340" 
                                height="220" alt="Thumbnail">
                            </div>
                            <div class="form-group">
                                <label>Content</label>
                                <p>{!! $articles->content !!}</p>
							</div>
							<div class="form-group">
								<label>Post By</label>
								<p><i class="fas fa-clock"></i> {{ $articles->postby }}</p>
							</div>
							<div class="form-group">
								<label>Post Date</label>
								<p><i class="fas fa-calendar-week"></i> {{ $articles->date }}, {{ $articles->time }}</p>
							</div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @endsection @section('js') @endsection