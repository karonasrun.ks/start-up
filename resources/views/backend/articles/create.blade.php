@extends('layouts.backend') @section('css')
<style>
    .note-editable,.card-block{
    		height: 300px !important;;
    	}
</style>
@endsection @section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Dashboard</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ url('article') }}">Article</a>
                        </li>
                        <li class="breadcrumb-item active">Create Article</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    <!-- Main content -->

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">Article
                            </h3>
                            <div class="card-tools col-sm-1">
                                <a href="{{ route('article.index') }}" class="btn btn-primary btn-block btn-sm"><i class="fas fa-list"></i> List</a>

                            </div>
                        </div>
                        <!-- /.card-header -->
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <form class="form-horizontal" ​​​ action="{{ route('article.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body pad">

                                <div class="form-group">
                                    <label for="exampleInputEmail1"> <i class="fas fa-book mr-1"></i> Title</label>
                                    <input type="text" class="form-control" name="title" id="exampleInputEmail1" required placeholder="Enter title">
                                </div>
                                <div class="form-group pull-left">
                                    <img src="{{ asset('img/thumbnail.jpg') }}" id="thumbnail-img-tag" class="img-thumbnail" alt="Thumbnail" style="width: 250px;height: 250px;">
                                    <input id="myInput" type="file" class=" form-control thumbnail-img" name="thumbnail" style="visibility:hidden" />
                                    <a href="#" class="btn btn-sm btn-primary" onclick="$('#myInput').click();"><i class="fas fa-file"></i> Thumbnail</a>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Short Description</label>
                                    <textarea class="form-control" cols="10" name="short_description"  required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Content</label>
                                    <textarea class="textarea" cols="10" name="content" placeholder="Place some text here" required style="width: 100%; height: 500px; font-size: 14px; border: 1px solid rgb(221, 221, 221); display: none;"></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select class="form-control" name="articletype">
												@foreach ($articleTypes as $articleType)
													<option value="{{ $articleType->id}}">{{ $articleType->article_type }}</option>
												@endforeach
											</select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Date</label>
                                            <input type="text" name="date" class="form-control" id="date" value="{{ date('d-m-Y') }}" placeholder="dd/mm/yyyy" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Time</label>
                                            <input type="text" name="time" class="form-control" id="time" value="{{ date('H:i A') }}" placeholder="hh:mm" required>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info btn-sm col-sm-1"><i class="fas fa-save"></i>  Save</button>
                                <button type="reset" class="btn btn-danger btn-sm col-sm-1"><i class="fas fa-trash-restore"></i>  Cancel</button>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection @section('js') 
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#thumbnail-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".thumbnail-img").change(function(){
        readURL(this);
    });
</script>
@endsection