@extends('layouts.backend') @section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Dashboard</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Articles</a>
                        </li>
                        <li class="breadcrumb-item active">Articles List</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
        {{-- --}}
        <div class="row">
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
                @endif
                <div class="card card-outline card-info">
                    <div class="card-header">
                        <h3 class="card-title">
                            Article
                            <small>List</small>
                        </h3>
                        <!-- tools box -->
                        <div class="card-tools">
                            <a href="{{ route('article.create') }}" class="btn btn-sm btn-primary">
                  <i class="fas fa-plus-circle"></i> Create New</a>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body pad">
                        <table id="example1" class="table table-bordered table-striped dataTable">
                            <thead>
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th>Article</th>
                                    <th style="width: 10%;">Thumbnail</th>
                                    <th style="width: 8%;">Post by</th>
                                    <th>Post date</th>
                                    <th style="width: 20%; height:10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($articles as $key => $article)
                                <tr>
                                    <td>{{ ++ $key}}</td>
                                    <td>{{ $article->title }}</td>
                                    <td><img src="{{ asset('storage/'.$article->thumbnail) }}" width="70" 
                                        height="70" alt="Thumbnail"></td>
                                    <td>{{ $article->postby }}</td>
                                    <td>{{ $article->date }} {{$article->time}}</td>
                                    <td>
                                        <div style="margin-bottom: 10px;">
                                            <form action="{{ url('article/status',$article->id) }}" method="post" style="display:inline">
                                                @csrf @if($article->active_status == 1)
                                                <input type="hidden" name="status" value="0">
                                                <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-toggle-off"></i> Deactivated</button> @else
                                                <input type="hidden" name="status" value="1">
                                                <button type="submit" class="btn btn-success btn-sm"><i class="fas fa-toggle-on"></i> Activated</button> @endif

                                            </form>
                                            <a href="{{ route('article.show',$article->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-folder"></i> Show</a>
                                        </div>
                                        <a href="{{ route('article.edit',$article->id) }}" class="btn btn-warning btn-sm"><i class="fas fa-pencil-alt"></i> Edit</a>
                                        <form action="{{ route('article.destroy',[$article->id]) }}" method="post" style="display:inline">
                                            {{ csrf_field() }} {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash-restore"></i> Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection @section('js')
<script type="text/javascript">
    $(function () {
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "fixedHeader": {
                "header": true,
                "headerOffset": $('#header').height()
            },
            "oLanguage": {
                "sSearch": "Filter results:"
            },
            "responsive": false,
            "select": true,
            "destroy": true,
        });
      });
</script>
@endsection