@extends('layouts.backend')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Dashboard</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item">
							<a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="{{ url('users') }}">User</a>
                        </li>
						<li class="breadcrumb-item active">Users List</li>
					</ol>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
    </section>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
        <p>{{ $message }}</p>
        </div>
    @endif
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-12">				
				<div class="card">
					<div class="card-header">
                        <h3 class="card-title">Users List
                        <a href="{{ route('user.create') }}" class="btn btn-success btn-sm pull-left"><i class="fas fa-plus"></i> Create New user</a>
                        </h3>
                    </div>
					<!-- /.card-header -->
					<div class="card-body">
						<table id="example1" class="table table-bordered table-striped dataTable">
							<thead>
								<tr>
									<th>No</th>
									<th>Photo</th>
									<th>Username</th>
									<td>Email</td>
                                    <th>User Type</th>
                                    <th>User Status</th>
                                    <th></th>
								</tr>
							</thead>
							<tbody>	
                            @foreach ($users as $i => $user)                                   
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $user->username}}</td>
                                <td>{{ $user->username}}</td>
                                <td>{{ $user->email}}</td>
                                <td>
                                @if(!empty($user->getRoleNames()))
                                    @foreach($user->getRoleNames() as $v)
                                    <span class="badge badge-success">{{ $v }}</span>
                                    @endforeach
                                @endif    
                                </td>
                                <td>{{ $user->active_status}}</td>
                                <td>
                                    <a href="{{ route('user.show',$user->id) }}" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Reset password"><i class="fas fa-key"></i> Show</a>
                                    <a href="{{ route('user.edit',$user->id) }}" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Status"><i class="fas fa-toggle-on"></i> Edit</a>           
                                    {!! Form::open(['method' => 'DELETE','route' => ['user.destroy', $user->id],'style'=>'display:inline']) !!}
                                    {{ Form::button('<i class="fa fa-trash"></i> submit', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm'] )  }}
                                {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>                        
					</div>
					<!-- /.card-body -->
				</div>
                <!-- /.card -->                
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
      
@endsection
@section('js')			
<script type="text/javascript">
$(function () {
    $('#example1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "fixedHeader": {
            "header": true,
            "headerOffset": $('#header').height()
        },
        "oLanguage": {
            "sSearch": "Filter results:"
        },
        "responsive": false,
        "select": true,
        "destroy": true,
    });
  });
</script>
@endsection