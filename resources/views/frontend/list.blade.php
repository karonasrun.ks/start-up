@extends('layouts.frontend')
@section('content')
<div role="main" class="main">
        

    <div class="container">
        <section class="{{ $style }}">
            <div class="container">            
                <div class="row">
                    <div class="col-md-12">
                       <p class="p">{{ $header }}</p>
                    </div>
                </div>
            </div>
        </section>
        <div class="row">
            <div class="col-md-3">
                <aside class="sidebar">
                    <h4 class="heading-primary">Categories</h4>
                    <ul class="nav nav-list mb-xl">
                        <li><a href="#">Design (2)</a></li>
                        <li class="active">
                            <a href="#">Photos (4)</a>
                            <ul>
                                <li><a href="#">Animals</a></li>
                                <li class="active"><a href="#">Business</a></li>
                                <li><a href="#">Sports</a></li>
                                <li><a href="#">People</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Videos (3)</a></li>
                        <li><a href="#">Lifestyle (2)</a></li>
                        <li><a href="#">Technology (1)</a></li>
                    </ul>

                </aside>
            </div>
            <div class="col-md-9">
                            <div class="well well-sm">
                                <strong>Category Title</strong>
                                <div class="btn-group">
                                    <a href="#" id="list" class="btn btn-default btn-sm"><span class="fas fa-th-list">
                                    </span> List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
                                        class="fas fa-th"></span> Grid</a>
                                </div>
                            </div>
                            <div id="products" class="row list-group">
                                <div class="item  col-xs-4 col-lg-4">
                                    <div class="thumbnail">
                                        <img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
                                        <div class="caption">
                                            <h4 class="group inner list-group-item-heading">
                                                Product title</h4>
                                            <p class="group inner list-group-item-text">
                                                Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                                sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                                <div class="row">
                                                    <div class="col-xs-6 col-md-6">
                                                        <p class="load">
                                                            $21.000</p>
                                                    </div>
                                                    <div class="col-xs-6 col-md-6">
                                                        <a class="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item  col-xs-4 col-lg-4">
                                    <div class="thumbnail">
                                        <img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
                                        <div class="caption">
                                            <h4 class="group inner list-group-item-heading">
                                                Product title</h4>
                                            <p class="group inner list-group-item-text">
                                                Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                                sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                                <div class="row">
                                                    <div class="col-xs-6 col-md-6">
                                                        <p class="load">
                                                            $21.000</p>
                                                    </div>
                                                    <div class="col-xs-6 col-md-6">
                                                        <a class="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item  col-xs-4 col-lg-4">
                                    <div class="thumbnail">
                                        <img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
                                        <div class="caption">
                                            <h4 class="group inner list-group-item-heading">
                                                Product title</h4>
                                            <p class="group inner list-group-item-text">
                                                Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                                sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                                <div class="row">
                                                    <div class="col-xs-6 col-md-6">
                                                        <p class="load">
                                                            $21.000</p>
                                                    </div>
                                                    <div class="col-xs-6 col-md-6">
                                                        <a class="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item  col-xs-4 col-lg-4">
                                    <div class="thumbnail">
                                        <img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
                                        <div class="caption">
                                            <h4 class="group inner list-group-item-heading">
                                                Product title</h4>
                                            <p class="group inner list-group-item-text">
                                                Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                                sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                                <div class="row">
                                                    <div class="col-xs-6 col-md-6">
                                                        <p class="load">
                                                            $21.000</p>
                                                    </div>
                                                    <div class="col-xs-6 col-md-6">
                                                        <a class="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item  col-xs-4 col-lg-4">
                                    <div class="thumbnail">
                                        <img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
                                        <div class="caption">
                                            <h4 class="group inner list-group-item-heading">
                                                Product title</h4>
                                            <p class="group inner list-group-item-text">
                                                Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                                sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                                <div class="row">
                                                    <div class="col-xs-6 col-md-6">
                                                        <p class="load">
                                                            $21.000</p>
                                                    </div>
                                                    <div class="col-xs-6 col-md-6">
                                                        <a class="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item  col-xs-4 col-lg-4">
                                    <div class="thumbnail">
                                        <img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
                                        <div class="caption">
                                            <h4 class="group inner list-group-item-heading">
                                                Product title</h4>
                                            <p class="group inner list-group-item-text">
                                                Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                                sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                            <div class="row">
                                                <div class="col-xs-6 col-md-6">
                                                    <p class="load">
                                                        $21.000</p>
                                                </div>
                                                <div class="col-xs-6 col-md-6">
                                                    <a class="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
            </div>
            
        </div>

    </div>

</div>
@endsection
