@extends('layouts.frontend')
@section('content')
<div role="main" class="main">

    <section class="page-header">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <ul class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">About Us</li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <h1>About Us</h1>
          </div>
        </div>
      </div>
    </section>

    <div class="container">
      <div class="row">
        <div class="col-md-12 center">
          <h2 class="word-rotator-title mb-lg">
            The New Way to 
            <strong class="inverted inverted-primary">
              <span class="word-rotate active" data-plugin-options="{'delay': 2000, 'animDelay': 300}">
                <span class="word-rotate-items" style="width: 132.547px; top: -84px;">
                  <span>success.</span>
                  <span>advance.</span>
                  <span>progress.</span>
                <span>success.</span></span>
              </span>				
            </strong>
          </h2>

          <p class="lead">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non <span class="alternative-font">metus.</span> pulvinar. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.
          </p>

          <hr class="tall">
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="feature-box feature-box-style-2">
            <div class="feature-box-icon">
              <i class="fa fa-group"></i>
            </div>
            <div class="feature-box-info">
              <h4 class="mb-none">Customer Support</h4>
              <p class="tall">Lorem ipsum dolor sit amet, consectetur adipiscing <span class="alternative-font">metus.</span> elit. Quisque rutrum pellentesque imperdiet.</p>
            </div>
          </div>
          <div class="feature-box feature-box-style-2">
            <div class="feature-box-icon">
              <i class="fa fa-file"></i>
            </div>
            <div class="feature-box-info">
              <h4 class="mb-none">HTML5 / CSS3 / JS</h4>
              <p class="tall">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="feature-box feature-box-style-2">
            <div class="feature-box-icon">
              <i class="fa fa-film"></i>
            </div>
            <div class="feature-box-info">
              <h4 class="mb-none">Sliders</h4>
              <p class="tall">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla.</p>
            </div>
          </div>
          <div class="feature-box feature-box-style-2">
            <div class="feature-box-icon">
              <i class="fa fa-check"></i>
            </div>
            <div class="feature-box-info">
              <h4 class="mb-none">Icons</h4>
              <p class="tall">Lorem ipsum dolor sit amet, consectetur adipiscing <span class="alternative-font">metus.</span> elit. Quisque rutrum pellentesque imperdiet.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="feature-box feature-box-style-2">
            <div class="feature-box-icon">
              <i class="fa fa-bars"></i>
            </div>
            <div class="feature-box-info">
              <h4 class="mb-none">Buttons</h4>
              <p class="tall">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla.</p>
            </div>
          </div>
          <div class="feature-box feature-box-style-2">
            <div class="feature-box-icon">
              <i class="fa fa-desktop"></i>
            </div>
            <div class="feature-box-info">
              <h4 class="mb-none">Lightbox</h4>
              <p class="tall">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    
    <section class="video section section-text-light section-video section-center mt-none" data-video-path="video/dark" data-plugin-video-background="" data-plugin-options="{'posterType': 'jpg', 'position': '50% 50%', 'overlay': true}"><div style="position: absolute; z-index: -1; top: 0px; left: 0px; bottom: 0px; right: 0px; overflow: hidden; background-size: cover; background-color: transparent; background-repeat: no-repeat; background-position: 50% 50%; background-image: none;"><video autoplay="" loop="" muted="" style="margin: auto; position: absolute; z-index: -1; top: 50%; left: 50%; transform: translate(-50%, -50%); visibility: visible; opacity: 1; width: 1351px; height: auto;"><source src="video/dark.mp4" type="video/mp4"><source src="video/dark.webm" type="video/webm"><source src="video/dark.ogv" type="video/ogg"></video></div><div class="video-overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="owl-carousel owl-theme nav-bottom rounded-nav mt-lg mb-none owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'items': 1, 'loop': false}">
              
              
            <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1110px;"><div class="owl-item active" style="width: 555px;"><div>
                <div class="col-md-12">
                  <div class="testimonial testimonial-style-6 testimonial-with-quotes mb-none">
                    <blockquote>
                      <p>Hello, I want to thank you for creating a great template and for the excellent and quick support and help that you have been providing to me as I begin to work with it.</p>
                    </blockquote>
                    <div class="testimonial-author">
                      <p><strong>John Smith</strong><span>CEO &amp; Founder - Okler</span></p>
                    </div>
                  </div>
                </div>
              </div></div><div class="owl-item" style="width: 555px;"><div>
                <div class="col-md-12">
                  <div class="testimonial testimonial-style-6 testimonial-with-quotes mb-none">
                    <blockquote>
                      <p>Just want to say Okler RULES. Provide great tech service for each template and allows me to become more knowledgeable as a designer.</p>
                    </blockquote>
                    <div class="testimonial-author">
                      <p><strong>John Smith</strong><span>CEO &amp; Founder - Okler</span></p>
                    </div>
                  </div>
                </div>
              </div></div></div></div><div class="owl-nav disabled"><div class="owl-prev"></div><div class="owl-next"></div></div><div class="owl-dots"><div class="owl-dot active"><span></span></div><div class="owl-dot"><span></span></div></div></div>
          </div>
        </div>
      </div>
    </section>

    
    <section class="call-to-action call-to-action-default with-button-arrow call-to-action-in-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="call-to-action-content">
              <h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website!</h3>
              <p>The <strong>#1 Selling</strong> HTML Site Template on ThemeForest</p>
            </div>
            <div class="call-to-action-btn">
              <a href="http://themeforest.net/item/porto-responsive-html5-template/4106987" target="_blank" class="btn btn-lg btn-primary">Buy Now!</a><span class="arrow hlb hidden-xs hidden-sm hidden-md appear-animation animated rotateInUpLeft appear-animation-visible" data-appear-animation="rotateInUpLeft" style="top: -12px;"></span>
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
  @endsection