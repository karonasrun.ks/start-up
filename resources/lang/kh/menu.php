<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'app_name' => 'Bee Luagh',
    'home' => 'ទំព័រដើម',
    'tours' => 'ដំណើរកំសាន្ត',
    'hotels' => 'សណ្ឋានគារ',
    'services' => 'សេវាកម្ម',
    'about_us' => 'អំពី​ពួក​យើង',
    'blog' => 'ប្លក់',
    'contact_us' => 'ការទំនាក់ទំនង',
    'khmer' => 'ខ្មែរ',
    'english' => 'អង់គ្លេស',
    'user' => 'អ្នកប្រើប្រាស់',
    'login' => 'ចូលប្រើប្រាស់',
    'register' => 'ចុះឈ្មោះ',
    'booking' => 'ការកក់សំបុត្រ',
    'search' => 'តើលោកអ្នកចង់ទៅកម្សាន្តកន្លែងណាខ្លះ?',
    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines Buttom
    |--------------------------------------------------------------------------
    */
    'news' => 'ព័តមានថ្មីៗ',
    'scenery' => 'កម្រងទេសភាព',
    'experience' => 'បទពិសោធន៏',
    'life' => 'មេរៀនជិវិត',
    'society' => 'ព័តមានសង្គម',
    'entertainment' => 'សិល្បៈ និងការកម្សាន្ត',
    'health' => 'សុខភាព​ និងសម្រស់',
    'sport' => 'ព័តមានកីឡា',
    /*
    |--------------------------------------------------------------------------
    | Menu Language BackEnd
    |--------------------------------------------------------------------------
    */
    'dashboard' => 'ផ្ទាំងព័តមាន',
    'article' => 'ព័តមាន',
    'type-article' => 'ប្រភេទនៃព័តមាន',
    'visitor' => 'អ្នកចូលទស្សនា',
    'security' => 'សុវត្តិភាព',
        'user-account' => 'អ្នកប្រើប្រាស់',
        'user-log' => 'ព៍តមានការចូលប្រើប្រាស់',
    'view-details' => 'មើលព័តមានលម្អិត',
];