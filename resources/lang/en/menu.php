<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'app_name' => 'Bee Luagh',
    'home' => 'Home',
    'tours' => 'Tours',
    'hotels' => 'Hotels',
    'services' => 'Services',
    'about_us' => 'About US',
    'blog' => 'Blog',
    'contact_us' => 'Contact US',
    'khmer' => 'Khmer',
    'english' => 'English',
    'user' => 'User',
    'login' => 'Login',
    'register' => 'Register',
    'booking' => 'Booking',
    'search' => 'Where do you want to go ?',
    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines Buttom
    |--------------------------------------------------------------------------
    */
    'news' => 'News',
    'scenery' => 'Scenery',
    'experience' => 'Experience',
    'life' => 'Life',
    'society' => 'Society',
    'entertainment' => 'Entertianment',
    'health' => 'Health & Beauti',
    'sport' => 'Sport',

    /*
    |--------------------------------------------------------------------------
    | Menu Language BackEnd
    |--------------------------------------------------------------------------
    */
    'dashboard' => 'Dashboard',
    'article' => 'Article',
    'type-article' => 'Article Type',
    'visitor' => 'Visitor',
    'security' => 'Security',
        'user-account' => 'Users',
        'user-log' => 'Users log',

    'view-details' => 'View Details',
];